﻿var pageNumber=1;

var aboutAuthor={image:"author.jpg",
                  name:"Tatiana Romankova",
                  about:"Graduated from Gomel State University named F.Skarina. Lecturer, Department of Informatics at the Gomel State Technical University "   


     }
var contact={
         name:"Tatiana Romankova",
         phone:"333333333",
         email:"tatianaromankova1@gmail.com",
         skype:"ran_717",  
         address:"Gomel 60let SSSR 77/77"
          
     }

var ladogaPhotos=["\\ladoga\\im048.jpg","\\ladoga\\im138.jpg","\\ladoga\\im193.jpg","\\ladoga\\im203.jpg","\\ladoga\\im232.jpg"]
var kareliaPhotos=["\\karelia\\img00137.jpg","\\karelia\\img153.jpg","\\karelia\\img173.jpg","\\karelia\\img00175_5.jpg","\\karelia\\img00352.jpg","\\karelia\\img391.jpg","\\karelia\\img406.jpg","\\karelia\\img00537.jpg",]
var bukovelPhotos=["\\bukovel\\img061.jpg","\\bukovel\\img077.jpg","\\bukovel\\img124.jpg","\\bukovel\\img139.jpg","\\bukovel\\img142.jpg","\\bukovel\\img170.jpg","\\bukovel\\img216.jpg"]

// The template to output  the post

var table=function(caption,firstCell,image,imageNumber,imageSize,secondCell){
    var element='<table><caption><i>'+caption+'</i></caption>'+
                 '<tr> <td>'+firstCell+'</td></tr>'+
                 '<tr><td><a href=""><img id ="img'+imageNumber+'" src=IMAGES\\'+image+
                 ' height='+imageSize.height+' width='+imageSize.width+
                 '></a></td><td rowspan=2 style="text-align:justify">'
                  +secondCell +'</td></tr></table>';
    return element;
} 

// The template to output  the photos

var photos=function(images,prefix){
     
    var element='<div id="large">'+

                     '<img src=IMAGES\\'+images[0]+'  /></div>'+
     '<ul id="thumbnail">';
             
   
    for(var i=0;i<images.length;i++){
             
            
             element=element+'<li><a id="'+prefix+i+'" href="#"><img src=IMAGES\\'+images[i]+'  /></a></li>';
              
             
              
            }
    element=element+'</ul>';

    return element;
}

//output full or short post

var writePost=function(post,idElement,state)
    {
     var writeArticle=post.article.substring(0,200);
     
     if(state=='full')
        writeArticle=post.article;
     
     
     
     if(state!='full') {
                          
                          writeArticle=writeArticle+'<br> <a id="aa'+post.id+'" href="#" return=false>Read more...</a> <br><br>';
                          
                                                 
                          
                          
                        }  
      var element=table(post.title,post.date,post.image,post.id,{height:100,width:100},writeArticle);
                       $(idElement).append(element);
     
    }

// The filter to output  the post by the set criterion
var filter=function(filterInfo,post){
   if(filterInfo.criterion=='all')
         writePost(post,'#cont','short');
   else
    if(filterInfo.criterion=='rubric')
        { if (filterInfo.rubric==post.rubric)     
               writePost(post,'#cont','short');
        }
     else
      if(filterInfo.criterion=='date')
        {if((new Date(filterInfo.daten))<=(new Date(post.date)) && (new Date(post.date))<=(new Date(filterInfo.datek)))
             writePost(post,'#cont','short');

        }
   
}
function setEqualHeight(idCol1,idCol2)
 {
  
  var h1=$(idCol1).height();
  $(idCol2).height(h1);      

 }

function SetParametersColumns()
{
    setEqualHeight('#cont','#rightCol');
    $('#rub').height($('#cont').height()-$('#fnd').height()-10);
    $('#cntnr').height($('#rightCol').height()+20);
    $('#wrap').height(70+$('#tp').height()+$('#nav').height()+$('#cntnr').height()+40);
    
    
}

function writeFullPost(post)
 {
    clearElement('cont');
    writePost(post,'#cont','full');
    SetParametersColumns();
 }
var pageButtons=function(pageCount){
  var pageButtons='<button id="prev" type="button">&lt;&lt;</button>';
            for(var i=1;i<=pageCount;i++){
            pageButtons=pageButtons+'<button id="n'+i+'" type="button">'+i+'</button>';
             }
            pageButtons=pageButtons+'<button id="next" type="button">&gt;&gt;</button><br>';
       return pageButtons;
}

var outputMain=function(filterInfo){
         
        $.getJSON('posts.json', function(data) {
            var pageCount= Math.floor(data.posts.length/3);
            
            $('#cont').append(pageButtons(pageCount));
           
            
                
            for(var i=0;i<data.posts.length;i++){
              
                 filter(filterInfo,data.posts[i]);
                 $('#aa'+data.posts[i].id).bind( 'click',data.posts[i],function(eventObject){writeFullPost(eventObject.data); });
                 showModal('img'+data.posts[i].id,'<img src=IMAGES\\'+data.posts[i].image+' height=600 width=600>');    
            }
           DisplayPage(pageNumber); 
           SetOnClickPageButton() 
           SetParametersColumns();
           
      });
            
    }


var outputAuthor=function(data,idElement){
	
         var element=table('<h2>'+data.name+'<h2>','',data.image,0,{height:300,width:200},data.about);
            $(idElement).append(element);
          
}

var outputContact=function(data,idElement)
   {
      var element='<div>  <h2><i>'+data.name +'</i></h2><br>'+
                   '<font size=5> phone:'+data.phone+'<br><br> e-mail:'+
                                    data.email+'<br><br> address:'+
                                    data.address+
                   '</font> </div>';
          $(idElement).append(element);
   }
function SetOnClick(images,prefix){
    
for(var i=0;i<images.length;i++)
           {
             
                       
               $('#'+prefix+i).bind( 'click',images[i],function(eventObject){writeLargePhoto(eventObject.data); });
              
            }
}

function SetOnClickPageButton(){
 
 var tableCount=$('#cont table').get().length;
 var pageCount= Math.floor(tableCount/4);
 if (pageCount*4<tableCount)
         pageCount=pageCount+1;
 $('#prev').bind( 'click',function(){ var numPrev=pageNumber-1;
                                      if(numPrev==0)
                                       numPrev=1;pageNumber=numPrev;
                                       DisplayPage(pageNumber);});   
  for(var i=1;i<=pageCount;i++)
           {
             
                        
               $('#n'+i).bind( 'click',function(){pageNumber=$(this).text();DisplayPage(pageNumber); });
              
            }
 $('#next').bind( 'click',function(){var numNext=pageNumber+1;
                                      var tableCount=$('#cont table').get().length;
                                      var pageCount= Math.floor(tableCount/4);
                                      if (pageCount*4<tableCount)
                                                 pageCount=pageCount+1;
                                     if(numNext>pageCount)
                                         numNext=pageCount;pageNumber=numNext;
                                         DisplayPage(pageNumber); });  
}

function DisplayPage(pageNumber){
  var tableCount=$('#cont table').get().length;
  var pageCount= Math.floor(tableCount/4);
  if (pageCount*4<tableCount)
     pageCount=pageCount+1;
  var n1=4*pageNumber-4;
  var n=4;
  var n2=4*pageNumber;
  if(n2>tableCount)
   {
    n=n2-tableCount;
    n2=tableCount;
   }
  console.info(pageNumber);
  
  $("#cont table") .css('display', 'none'); 
  $("#cont table") .slice (n1,n2) .css('display', 'block'); 
  SetParametersColumns();
}


function loadPage() {
    
    outputMain({criterion:'all'});
   
   $(document).ready(function(){ DisplayPage(1);});
    SetParametersColumns(); 
    $('#date1').datepicker();
    $('#date2').datepicker();
    $('button').button();
    $('#kayak').bind( 'click',{criterion:'rubric',rubric:'Kayaking'},function(eventObject){writeFilterResult(eventObject.data); });
    $('#skiing').bind( 'click',{criterion:'rubric',rubric:'Mountain skiing'},function(eventObject){writeFilterResult(eventObject.data); });
    $('#mountain').bind( 'click',{criterion:'rubric',rubric:'Mountain tourism'},function(eventObject){writeFilterResult(eventObject.data); });
    $('#raft').bind( 'click',{criterion:'rubric',rubric:'Rafting'},function(eventObject){writeFilterResult(eventObject.data); });
    $('#dat').bind( 'click',{criterion:'date',rubric:'',daten:'',datek:''},
                             function(eventObject)
                                {eventObject.data.daten=$('#date1').val();
                                 eventObject.data.datek=$('#date2').val();
                                 writeFilterResult(eventObject.data);
                                 $('#date1').val('');
                                 $('#date2').val('');});
   
     $('#ladoga').click(function(){showPhotos(ladogaPhotos,'ladoga')});
     $('#karelia').click(function(){showPhotos(kareliaPhotos,'karelia')});
     $('#bukovel').click(function(){showPhotos(bukovelPhotos,'bukovel')});
     
     
}   

   

function clearElement(idElement)
  {
     document.getElementById(idElement).innerHTML = '';
   
   }

function writeInfoAboutAuthor(){
    clearElement('cont');
    outputAuthor(aboutAuthor,'#cont');
    SetParametersColumns();
    
}

function writeContact(){
    clearElement('cont');
    outputContact(contact,'#cont');
    SetParametersColumns();
}

function writeFilterResult(filterInfo){
    clearElement('cont');
    pageNumber=1;
    outputMain(filterInfo);
    SetParametersColumns();
    
}

function showPhotos(images,prefix){
   clearElement('cont');
   
   $('#cont').append(photos(images,prefix));
   SetOnClick(images,prefix);
   SetParametersColumns();
}

function writeLargePhoto(imgName){
   clearElement('large');
   console.info(this.imgName);
   var img='<img src=IMAGES\\'+imgName+'  />';
   $('#large').append(img);

}
//displaying a modal windows for zoomed picture
function showModal(idElement,content){
   
	$('#'+idElement).click( function(event){ 
		event.preventDefault();  
		$('#overlay').fadeIn(400, 
		 	function(){ 
				$('#modal') 
					.css('display', 'block') 
					.animate({opacity: 1, top: '50%'}, 200);
                                 $('#modal').append(content);     
		});
	});
        $('#overlay').click( function(){ 
		$('#modal')
			.animate({opacity: 0, top: '45%'}, 200,  
				function(){
                                        clearElement('modal'); 
					$(this).css('display', 'none'); 
					$('#overlay').fadeOut(400); 
				}
			);
	});
}

//response to change the window size
window.onresize=function(){
    SetParametersColumns();    
}
   